#About
This is the repository for the code used to create an school commuting emissions model featured in this paper: 

Singleton, A. (2013) A GIS Approach to Modelling $$CO_2$$ Emissions Associated with the Pupil-School Commute. International Journal of Geographical Information Science, 28(2):256–273.

In addition to [R](http://www.r-project.org/), the [Routino](http://www.routino.org/) library is also required. 

This work was created as part of an ESRC grant ES/K007459/1.

A basic example of using R with Routino can be found [here](http://rpubs.com/alexsingleton/routino).
